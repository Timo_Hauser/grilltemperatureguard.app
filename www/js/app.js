// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'chart.js', 'starter.controllers'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (cordova.platformId === 'ios' && window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/tabs.html',
        controller: 'AppCtrl'
    })

    .state('app.home', {
        url: '/home',
        views: {
            'home': {
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
            }
        }
    })

    .state('app.sensors', {
            url: '/sensors',
            views: {
                'sensors': {
                    templateUrl: 'templates/sensors.html',
                    controller: 'SensorsCtrl'
                }
            }
        })
        .state('app.sensor', {
            url: '/sensors/:sensorId',
            views: {
                'sensors': {
                    templateUrl: 'templates/sensor.html',
                    controller: 'SensorCtrl'
                }
            }
        })

    .state('app.servos', {
            url: '/servos',
            views: {
                'servos': {
                    templateUrl: 'templates/servos.html',
                    controller: 'ServosCtrl'
                }
            }
        })
        .state('app.servo', {
            url: '/servos/:servoId',
            views: {
                'servos': {
                    templateUrl: 'templates/servo.html',
                    controller: 'ServoCtrl'
                }
            }
        })

    .state('app.timer', {
        url: '/timer',
        views: {
            'timer': {
                templateUrl: 'templates/timer.html'
            }
        }
    })

    .state('app.settings', {
        url: '/settings',
        views: {
            'settings': {
                templateUrl: 'templates/settings.html',
                controller: 'SettingsCtrl'
            }
        }
    })

    .state('app.about', {
        url: '/about',
        views: {
            'about': {
                templateUrl: 'templates/about.html'
            }
        }
    })

    ;
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/home');
});