angular.module('starter.controllers', [])

.factory('$localstorage', ['$window', function($window) {
    return {
        set: function(key, value) {
            $window.localStorage[key] = value;
        },
        get: function(key, defaultValue) {
            return $window.localStorage[key] || defaultValue || false;
        },
        setObject: function(key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function(key) {
            if ($window.localStorage[key] != undefined)
                return JSON.parse($window.localStorage[key] || false);

            return false;
        },
        remove: function(key) {
            $window.localStorage.removeItem(key);
        },
        clear: function() {
            $window.localStorage.clear();
        }
    }
}])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

    $scope.platform = ionic.Platform.platform();
    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});
})

.controller('HomeCtrl', function($scope, $http, $localstorage, $interval) {
    $scope.load = function() {
        host = 'http://' + $localstorage.get('RaspberryIp') + '/';
        $http.get(host + 'sensors').then(function(response) {
            $scope.sensors = response.data
        });
        $http.get(host + 'sensors/values/current').then(function(response) {
            $scope.sensor_values = response.data
        });
        $http.get(host + 'servos').then(function(response) {
            $scope.servos = response.data
        });
        $http.get(host + 'servos/values/current').then(function(response) {
            $scope.servo_values = response.data
        })
    }
    $interval(function() { $scope.load(); }, 5000);
    $scope.$on('$ionicView.enter', function(e) {
        $scope.load();
    });
})

.controller('SensorsCtrl', function($scope, $http, $localstorage, $interval) {
        $scope.load = function() {
            host = 'http://' + $localstorage.get('RaspberryIp') + '/';
            $http.get(host + 'sensors').then(function(response) {
                $scope.sensors = response.data
            });
            $http.get(host + 'sensors/values/current').then(function(response) {
                $scope.sensor_values = response.data
            });
        }
        $interval(function() { $scope.load(); }, 5000);
        $scope.$on('$ionicView.enter', function(e) {
            $scope.load();
        });
        $scope.add = function() {
            host = 'http://' + $localstorage.get('RaspberryIp') + '/';
            $http.post(host + 'sensors', {
                'name': '',
                'pin': 0,
                'activeForRegulation': false
            }).then(function(response) {
                $scope.sensors.push(response.data)
            })
        }
    })
    .controller('SensorCtrl', function($scope, $stateParams, $http, $localstorage, $state) {
        $scope.chart_data = [];
        $scope.chart_labels = [];
        $scope.$on('$ionicView.enter', function(e) {
            host = 'http://' + $localstorage.get('RaspberryIp') + '/';
            $http.get(host + 'sensors/' + $stateParams.sensorId).then(function(response) {
                $scope.sensor = response.data
                $scope.chart_series = [$scope.sensor.name];
            });
            $http.get(host + 'sensors/' + $stateParams.sensorId + '/values').then(function(response) {
                $scope.sensor_values = response.data;

                function getValue(item, index) {
                    return item.value;
                }

                function getLabel(item, index) {
                    return item.id;
                }
                $scope.chart_data = [$scope.sensor_values.map(getValue)];
                $scope.chart_labels = $scope.sensor_values.map(getLabel);
            });
        });
        $scope.save = function() {
            $http.put(host + 'sensors/' + $stateParams.sensorId, $scope.sensor).then(function(response) {
                $scope.sensor = response.data
            });
        }
        $scope.remove = function() {
            host = 'http://' + $localstorage.get('RaspberryIp') + '/';
            $http.delete(host + 'sensors/' + $scope.sensor.id).then(function(response) {
                $state.go('app.sensors');
            });
        }
    })

.controller('ServosCtrl', function($scope, $http, $localstorage, $interval) {
        $scope.load = function() {
            host = 'http://' + $localstorage.get('RaspberryIp') + '/';
            $http.get(host + 'servos').then(function(response) {
                $scope.servos = response.data
            });
            $http.get(host + 'servos/values/current').then(function(response) {
                $scope.servo_values = response.data
            })
        }
        $interval(function() { $scope.load(); }, 5000);
        $scope.$on('$ionicView.enter', function(e) {
            $scope.load();
        });
        $scope.add = function() {
            host = 'http://' + $localstorage.get('RaspberryIp') + '/';
            $http.post(host + 'servos', {
                'name': '',
                'pin': 0,
                'upperLimit': 100,
                'lowerLimit': 0,
                'targetValue': 50
            }).then(function(response) {
                $scope.servos.push(response.data)
            })
        }
    })
    .controller('ServoCtrl', function($scope, $stateParams, $http, $localstorage, $state) {
        $scope.chart_data = [
            []
        ];
        $scope.chart_series = [];
        $scope.chart_labels = [];
        $scope.$on('$ionicView.enter', function(e) {
            host = 'http://' + $localstorage.get('RaspberryIp') + '/';
            $http.get(host + 'servos/' + $stateParams.servoId).then(function(response) {
                $scope.servo = response.data;
                $scope.chart_series = [$scope.servo.name];
            });
            $http.get(host + 'servos/' + $stateParams.servoId + '/values').then(function(response) {
                $scope.servo_values = response.data;

                function getValue(item, index) {
                    return item.value;
                }
                $scope.chart_data = [$scope.servo_values.map(getValue)];
            });
        });
        $scope.save = function() {
            $http.put(host + 'servos/' + $stateParams.servoId, $scope.servo).then(function(response) {
                $scope.servo = response.data
            })
        }
        $scope.remove = function() {
            host = 'http://' + $localstorage.get('RaspberryIp') + '/';
            $http.delete(host + 'servos/' + $scope.servo.id).then(function(response) {
                $state.go('app.servos');
            })
        }
    })

.controller('SettingsCtrl', function($scope, $localstorage, $http) {
    $scope.default = {
        raspberryIp: '',
        refreshRate: 30
    }
    $scope.pi = {
        sensors: 15,
        servos: 30,
        servoIncrement: 0.5,
        temperature: 120,
    }
    $scope.$on('$ionicView.enter', function(e) {
        host = 'http://' + $localstorage.get('RaspberryIp') + '/';

        $scope.default.raspberryIp = $localstorage.get('RaspberryIp');
        $scope.default.refreshRate = $localstorage.get('RefreshRate', '15');

        $http.get(host + 'settings/value/temperature').then(function(response) {
            $scope.pi.temperature = response.data
        });
        $http.get(host + 'settings/value/sensorReadRate').then(function(response) {
            $scope.pi.sensors = response.data
        });
        $http.get(host + 'settings/value/servoWriteRate').then(function(response) {
            $scope.pi.servos = response.data
        });
        $http.get(host + 'settings/value/servoStepWidth').then(function(response) {
            $scope.pi.servoIncrement = response.data
        });
    })
    $scope.save = function() {
        $localstorage.set('RaspberryIp', $scope.default.raspberryIp);
        $localstorage.set('RefreshRate', $scope.default.refreshRate);

        $http.put(host + 'settings/value/temperature?value=' + $scope.pi.temperature);
        $http.put(host + 'settings/value/sensorReadRate?value=' + $scope.pi.sensors);
        $http.put(host + 'settings/value/servoWriteRate?value=' + $scope.pi.servos);
        $http.put(host + 'settings/value/servoStepWidth?value=' + $scope.pi.servoIncrement);
    }
});